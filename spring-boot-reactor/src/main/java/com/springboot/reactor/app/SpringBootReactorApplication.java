package com.springboot.reactor.app;

import java.time.Duration;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CountDownLatch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.springboot.reactor.app.models.Coments;
import com.springboot.reactor.app.models.Users;
import com.springboot.reactor.app.models.UsersComents;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@SpringBootApplication
public class SpringBootReactorApplication implements CommandLineRunner {

	private static final Logger log = LoggerFactory.getLogger(SpringBootReactorApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(SpringBootReactorApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		// sampleIterable();
		// sampleFlatMap();
		// sampleToString();
		// sampleToCollectListMono();
		// sampleUsersComentsFlatMap();
		// sampleUsersComentsFlatMapZipWith();
		// sampleUsersComentsFlatMapZipWithTwo();
		// sampleZipWithRages();
		// sampleDelay();
		// sampleDelayTwo();
		// sampleDelayInfinit();
		// sampleDelayToCreate();
		sampleContraPresion();

	}

	// public Users createUser() {
	// return new Users("Osvaldo", "Mata");

	public void sampleContraPresion() {

		Flux.range(1, 10).log().limitRate(2)
				.subscribe(/*
							 * new Subscriber<Integer>() {
							 * 
							 * private Subscription s;
							 * 
							 * private Integer limit = 5; private Integer
							 * consumido = 0;
							 * 
							 * @Override public void onSubscribe(Subscription s)
							 * { this.s = s; s.request(limit); }
							 * 
							 * @Override public void onNext(Integer t) {
							 * 
							 * log.info(t.toString()); consumido++; if
							 * (consumido == limit) { consumido = 0;
							 * s.request(limit); }
							 * 
							 * }
							 * 
							 * @Override public void onError(Throwable t) { //
							 * TODO Auto-generated method stub
							 * 
							 * }
							 * 
							 * @Override public void onComplete() { // TODO
							 * Auto-generated method stub
							 * 
							 * } }
							 */);

	}

	public void sampleDelayToCreate() {

		Flux.create(emitter -> {
			Timer timer = new Timer();
			timer.schedule(new TimerTask() {
				private Integer contador = 0;

				@Override
				public void run() {

					emitter.next(++contador);
					if (contador == 10) {
						timer.cancel();
						emitter.complete();
					}
					if (contador == 5) {
						timer.cancel();
						emitter.error(new InterruptedException("Error se ha detenido el flux en 5!"));
					}

				}
			}, 1000, 1000);
		}).subscribe(next -> log.info(next.toString()), error -> log.info(error.getMessage()),
				() -> log.info("Hemos Terminado"));

	}

	public void sampleDelayInfinit() throws InterruptedException {

		CountDownLatch latch = new CountDownLatch(1);

		Flux.interval(Duration.ofSeconds(1)).doOnTerminate(latch::countDown).flatMap(i -> {
			if (i >= 5) {
				return Flux.error(new InterruptedException("Solo hasta 5!"));
			}
			return Flux.just(i);
		}).map(i -> "Hola" + i).retry(2).subscribe(s -> log.info(s), e -> log.error(e.getMessage()));

		latch.await();

	}

	public void sampleDelayTwo() throws InterruptedException {

		Flux<Integer> range = Flux.range(1, 12).delayElements(Duration.ofSeconds(12))
				.doOnNext(i -> log.info(i.toString()));
		// range.subscribe();
		range.blockLast();/* No recomedable */

		Thread.sleep(13000);

	}

	public void sampleDelay() {

		Flux<Integer> range = Flux.range(1, 12);
		Flux<Long> delay = Flux.interval(Duration.ofSeconds(1));

		range.zipWith(delay, (ra, de) -> ra).doOnNext(i -> log.info(i.toString())).blockLast();

	}

	public void sampleZipWithRages() {

		// Flux<Integer> range = Flux.range(0, 4);
		Flux.just(1, 2, 3, 4).map(i -> (i * 2))
				.zipWith(Flux.range(0, 4) /* range */,
						(uno, dos) -> String.format("Primer Flux: %d, Segundo Flux: %d", uno, dos))
				.subscribe(texto -> log.info(texto));

	}

	public void sampleUsersComentsFlatMapZipWithTwo() {

		Mono<Users> userMono = Mono.fromCallable(() -> new Users("Osvaldo", "Mata"));

		Mono<Coments> comentsUserMono = Mono.fromCallable(() -> {
			Coments coments = new Coments();
			coments.addComents("Hola como estas");
			coments.addComents("Sin palabras");
			coments.addComents("Estoy muy bien");
			coments.addComents("Gracias por venir");
			return coments;
		});

		Mono<UsersComents> userWithConments = userMono.zipWith(comentsUserMono).map(tuple -> {
			Users u = tuple.getT1();
			Coments c = tuple.getT2();
			return new UsersComents(u, c);
		});
		userWithConments.subscribe(uc -> log.info(uc.toString()));

	}

	public void sampleUsersComentsFlatMapZipWith() {

		Mono<Users> userMono = Mono.fromCallable(() -> new Users("Osvaldo", "Mata"));

		Mono<Coments> comentsUserMono = Mono.fromCallable(() -> {
			Coments coments = new Coments();
			coments.addComents("Hola como estas");
			coments.addComents("Sin palabras");
			coments.addComents("Estoy muy bien");
			coments.addComents("Gracias por venir");
			return coments;
		});

		Mono<UsersComents> userWithConments = userMono.zipWith(comentsUserMono,
				(user, comentsUser) -> new UsersComents(user, comentsUser));
		userWithConments.subscribe(uc -> log.info(uc.toString()));

	}

	public void sampleUsersComentsFlatMap() {

		Mono<Users> userMono = Mono.fromCallable(() -> new Users("Osvaldo", "Mata"));

		Mono<Coments> comentsUserMono = Mono.fromCallable(() -> {
			Coments coments = new Coments();
			coments.addComents("Hola como estas");
			coments.addComents("Sin palabras");
			coments.addComents("Estoy muy bien");
			coments.addComents("Gracias por venir");
			return coments;
		});

		userMono.flatMap(u -> comentsUserMono.map(c -> new UsersComents(u, c)))
				.subscribe(uc -> log.info(uc.toString()));

	}

	public void sampleToCollectListMono() throws Exception {

		List<Users> usersList = new ArrayList<>();
		usersList.add(new Users("Osvaldo", "Mata"));
		usersList.add(new Users("Michelle", "Vega"));
		usersList.add(new Users("Areli", "Morenno"));
		usersList.add(new Users("Anabel", "Gonzalez"));
		usersList.add(new Users("Maria", "Fulana"));
		usersList.add(new Users("Dario", "Sandoval"));
		usersList.add(new Users("Alin", "Merengana"));
		usersList.add(new Users("Bruce", "Willis"));

		Flux.fromIterable(usersList).collectList().subscribe(user -> {
			user.forEach(l -> log.info(l.toString()));
		});
	}

	public void sampleToString() throws Exception {

		List<Users> usersList = new ArrayList<>();
		usersList.add(new Users("Osvaldo", "Mata"));
		usersList.add(new Users("Michelle", "Vega"));
		usersList.add(new Users("Areli", "Morenno"));
		usersList.add(new Users("Anabel", "Gonzalez"));
		usersList.add(new Users("Maria", "Fulana"));
		usersList.add(new Users("Dario", "Sandoval"));
		usersList.add(new Users("Alin", "Merengana"));
		usersList.add(new Users("Bruce", "Willis"));

		Flux.fromIterable(usersList)
				.map(user -> user.getName().toUpperCase().concat(" ").concat(user.getLastName().toUpperCase()))
				.flatMap(name -> {
					if (name.contains("bruce".toUpperCase())) {
						return Mono.just(name);
					} else {
						return Mono.empty();
					}
				}).map(name -> {
					return name.toLowerCase();
				}).subscribe(e -> log.info(e.toString()));
	}

	public void sampleFlatMap() throws Exception {

		List<String> usersList = new ArrayList<>();
		usersList.add("Osvaldo Mata");
		usersList.add("Michelle Vega");
		usersList.add("Areli Morenno");
		usersList.add("Anabel Gonzalez");
		usersList.add("Maria Fulana");
		usersList.add("Dario Sandoval");
		usersList.add("Alin Merengana");
		usersList.add("Bruce Willis");

		Flux.fromIterable(usersList)
				.map(name -> new Users(name.split(" ")[0].toUpperCase(), name.split(" ")[1].toUpperCase()))
				.flatMap(user -> {
					if (user.getName().equalsIgnoreCase("buce")) {
						return Mono.just(user);
					} else {
						return Mono.empty();
					}
				}).map(user -> {
					String nombre = user.getName().toLowerCase();
					user.setName(nombre);
					return user;
				}).subscribe(e -> log.info(e.toString()));
	}

	public void sampleIterable() throws Exception {

		// Flux<String> names = Flux.just("Osvaldo", "Michelle", "Anabel",
		// "Maria", "Dario").doOnNext(System.out::println);

		List<String> usersList = new ArrayList<>();
		usersList.add("Osvaldo Mata");
		usersList.add("Michelle Vega");
		usersList.add("Areli Morenno");
		usersList.add("Anabel Gonzalez");
		usersList.add("Maria Fulana");
		usersList.add("Dario Sandoval");
		usersList.add("Alin Merengana");
		usersList.add("Bruce Willis");

		// Flux<String> names = Flux.just("Osvaldo Mata", "Michelle Vega",
		// "Areli Morenno", "Anabel Gonzalez",
		// "Maria Fulana", "Dario Sandoval", "Alin Merengana", "Bruce Willis");
		Flux<String> names = Flux.fromIterable(usersList);
		Flux<Users> users = names.map(no -> new Users(no.split(" ")[0].toUpperCase(), no.split(" ")[1].toUpperCase()))
				.filter(no -> no.getName().toLowerCase().equals("bruce")).doOnNext(n -> {
					if (n == null) {
						throw new RuntimeException("No se pueden tener nombre vacios");
					}
					System.out.println(n.getName().concat(" ").concat(n.getLastName()));
				}).map(no -> {
					String nombre = no.getName().toLowerCase();
					no.setName(nombre);
					return no;
				});

		// names.subscribe(log::info);
		users.subscribe(e -> log.info(e.toString()), error -> log.error(error.getMessage()), new Runnable() {

			@Override
			public void run() {
				log.info("Ha finalizado la ejecucion del observable con éxito");
			}
		});

	}

}
