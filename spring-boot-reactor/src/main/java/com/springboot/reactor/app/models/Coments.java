package com.springboot.reactor.app.models;

import java.util.ArrayList;
import java.util.List;

public class Coments {

	private List<String> coments;

	public Coments() {
		this.coments = new ArrayList<>();
	}

	public void addComents(String coments) {
		this.coments.add(coments);
	}

	@Override
	public String toString() {
		return "Coments = " + coments;
	}

}
