package com.springboot.reactor.app.models;

public class Users {

	private String name;
	private String lastName;

	public Users(String name, String lastName) {
		this.name = name;
		this.lastName = lastName;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String toString() {
		return "Users [name=" + name + ", lastName=" + lastName + "]";
	}

}
