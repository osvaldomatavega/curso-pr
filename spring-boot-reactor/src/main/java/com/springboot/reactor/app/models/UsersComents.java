package com.springboot.reactor.app.models;

public class UsersComents {

	private Users users;
	private Coments coments;

	public UsersComents(Users users, Coments coments) {
		this.users = users;
		this.coments = coments;
	}

	@Override
	public String toString() {
		return "UsersComents [users=" + users + ", coments=" + coments + "]";
	}

}
