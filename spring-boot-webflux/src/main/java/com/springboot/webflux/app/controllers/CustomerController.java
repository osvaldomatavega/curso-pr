package com.springboot.webflux.app.controllers;

import java.net.URI;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.springboot.webflux.app.dao.service.interfaces.ICustomerService;
import com.springboot.webflux.app.models.Customer;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/customer/api")
public class CustomerController {

	@Autowired
	private ICustomerService service;

	@PostMapping
	public Mono<ResponseEntity<Customer>> createCustomer(@RequestBody Customer input) {
		if (!StringUtils.isEmpty(input)) {
			input.setId(UUID.randomUUID().toString());
		}
		return service.createCustomer(input)
				.map(c -> ResponseEntity.created(URI.create("/customer/api".concat(c.getId())))
						.contentType(MediaType.APPLICATION_JSON).body(c))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@GetMapping("/{id}")
	public Mono<ResponseEntity<Customer>> findCustomerById(@PathVariable(name = "id") String id) {
		return service.findCustomerById(id)
				.map(c -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(c))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@GetMapping
	public Mono<ResponseEntity<Flux<Customer>>> findAllCustomers() {
		return Mono.just(ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(service.findAllCustomers()));
	}

	@PutMapping("/{id}")
	public Mono<ResponseEntity<Customer>> updateCustomer(@PathVariable(name = "id") String id,
			@RequestBody Customer input) {
		return service.findCustomerById(id).flatMap(c -> {
			c.setAge(input.getAge());
			c.setBirthDate(input.getBirthDate());
			c.setLastName(input.getLastName());
			c.setName(input.getName());
			return service.updateCustomer(c);
		}).map(c -> ResponseEntity.ok().contentType(MediaType.APPLICATION_JSON).body(c))
				.defaultIfEmpty(ResponseEntity.notFound().build());
	}

	@DeleteMapping("/{id}")
	public Mono<ResponseEntity<Object>> deleteCustomer(@PathVariable(name = "id") String id) {
		return service.findCustomerById(id).flatMap(c -> {
			return service.deleteCustomer(c).then(Mono.just(ResponseEntity.noContent().build()));
		}).defaultIfEmpty(ResponseEntity.notFound().build());
	}

}
