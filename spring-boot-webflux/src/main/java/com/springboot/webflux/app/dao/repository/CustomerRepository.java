package com.springboot.webflux.app.dao.repository;

import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;

import com.springboot.webflux.app.models.Customer;

@Repository
public interface CustomerRepository extends ReactiveMongoRepository<Customer, String> {

}
