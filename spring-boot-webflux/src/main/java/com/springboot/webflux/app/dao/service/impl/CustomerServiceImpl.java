package com.springboot.webflux.app.dao.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.springboot.webflux.app.dao.repository.CustomerRepository;
import com.springboot.webflux.app.dao.service.interfaces.ICustomerService;
import com.springboot.webflux.app.models.Customer;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CustomerServiceImpl implements ICustomerService {

	@Autowired
	private CustomerRepository repository;

	@Override
	public Flux<Customer> findAllCustomers() {
		return repository.findAll();
	}

	@Override
	public Mono<Customer> findCustomerById(String id) {
		return repository.findById(id);
	}

	@Override
	public Mono<Customer> createCustomer(Customer input) {
		return repository.save(input);
	}

	@Override
	public Mono<Customer> updateCustomer(Customer input) {
		return repository.save(input);
	}

	@Override
	public Mono<Void> deleteCustomer(Customer input) {
		return repository.delete(input);
	}

}
