package com.springboot.webflux.app.dao.service.interfaces;

import com.springboot.webflux.app.models.Customer;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface ICustomerService {

	Flux<Customer> findAllCustomers();

	Mono<Customer> findCustomerById(String id);

	Mono<Customer> createCustomer(Customer input);

	Mono<Customer> updateCustomer(Customer input);

	Mono<Void> deleteCustomer(Customer input);

}
