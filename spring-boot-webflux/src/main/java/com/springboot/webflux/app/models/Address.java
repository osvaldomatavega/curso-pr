package com.springboot.webflux.app.models;

import java.io.Serializable;

import lombok.Data;

@Data
public class Address implements Serializable {

	private static final long serialVersionUID = -7313812711453408089L;

	private String street;
	private String number;
	private String municipality;
	private String state;
	private String zip_code;

}
