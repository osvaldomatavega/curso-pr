package com.springboot.webflux.app.models;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class Card implements Serializable {

	private static final long serialVersionUID = 3396259459571581390L;

	private String noCard;
	private String type;
}
