package com.springboot.webflux.app.models;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.format.annotation.DateTimeFormat;

import lombok.Data;

@Data
@Document(collection = "customers")
public class Customer implements Serializable {

	private static final long serialVersionUID = -897675343646472652L;

	@Id
	private String id;

	private String name;
	private String lastName;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date birthDate;
	private Integer age;
	private List<Card> cards;
	private Address address;

}
