package com.springboot.webflux.app;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.springboot.webflux.app.models.Address;
import com.springboot.webflux.app.models.Card;
import com.springboot.webflux.app.models.Customer;

public class Factory {

	public static Customer createCustomer() throws Exception {

		final String birthDate = "1998-09-24";

		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		final Date birthDateFormatter = formatter.parse(birthDate);

		Customer customer = new Customer();

		customer.setName("Osvaldo");
		customer.setLastName("Mata Vega");
		customer.setBirthDate(birthDateFormatter);

		Address address = new Address();

		address.setMunicipality("Nicolas Romero");
		address.setNumber("5");
		address.setState("Mexico");
		address.setStreet("Silverio Perez");
		address.setZip_code("54477");

		customer.setAddress(address);

		List<Card> cards = new ArrayList<>();

		cards.add(new Card("4325 5421 5992 3585", "Credito"));
		cards.add(new Card("8529 9594 9594 5842", "Debito"));

		customer.setCards(cards);

		return customer;
	}

}
