package com.springboot.webflux.app;

import java.util.Collections;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.springboot.webflux.app.dao.repository.CustomerRepository;
import com.springboot.webflux.app.models.Customer;

import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class SpringBootWebfluxApplicationTests {

	@Autowired
	private WebTestClient client;

	@Value("${config.base.endpoint}")
	private String url;

	@Autowired
	private CustomerRepository repository;

	@Test
	public void createCustomerTest() throws Exception {

		Customer request = Factory.createCustomer();

		client.post().uri(url).accept(MediaType.APPLICATION_JSON).body(Mono.just(request), Customer.class).exchange();

	}

	@Test
	public void updateCustomerTest() throws Exception {

		Customer customer = Factory.createCustomer();
		customer.setId("3");
		Mono<Customer> response = repository.save(customer);

		client.put().uri(url + "/{id}", Collections.singletonMap("id", response.block().getId()))
				.accept(MediaType.APPLICATION_JSON).body(Mono.just(customer), Customer.class).exchange();

	}

	@Test
	public void findAllCustomerTest() {

		client.get().uri(url).accept(MediaType.APPLICATION_JSON).exchange();

	}

	@Test
	public void findCustomerByIdTest() throws Exception {

		Customer customer = Factory.createCustomer();
		customer.setId("2");
		Mono<Customer> response = repository.save(customer);

		client.get().uri(url + "/{id}", Collections.singletonMap("id", response.block().getId()))
				.accept(MediaType.APPLICATION_JSON).exchange();

	}

	@Test
	public void deleteCustomerTest() throws Exception {

		Customer customer = Factory.createCustomer();
		customer.setId("1");
		Mono<Customer> response = repository.save(customer);

		client.delete().uri(url + "/{id}", Collections.singletonMap("id", response.block().getId()))
				.accept(MediaType.APPLICATION_JSON).exchange();

	}

}
