package com.springboot.webflux.app.service.impl;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import com.springboot.webflux.app.Factory;
import com.springboot.webflux.app.dao.repository.CustomerRepository;
import com.springboot.webflux.app.dao.service.impl.CustomerServiceImpl;
import com.springboot.webflux.app.models.Customer;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RunWith(MockitoJUnitRunner.class)
public class CustomerServiceImplTest {

	@InjectMocks
	private CustomerServiceImpl service;

	@Mock
	private CustomerRepository repository;

	@Test
	public void findAllCustomerTest() {

		Mockito.when(repository.findAll()).thenReturn(Flux.empty());

		Flux<Customer> response = service.findAllCustomers();
		Assert.assertNotNull(response);

		Mockito.verify(repository, Mockito.times(1)).findAll();

	}

	@Test
	public void findCustomerByIdTest() {

		Mockito.when(repository.findById(Mockito.anyString())).thenReturn(Mono.empty());

		Mono<Customer> response = service.findCustomerById("1");
		Assert.assertNotNull(response);

		Mockito.verify(repository, Mockito.times(1)).findById(Mockito.anyString());

	}

	@Test
	public void createCustomerTest() throws Exception {

		Mockito.when(repository.save(Mockito.any(Customer.class))).thenReturn(Mono.empty());

		Customer request = Factory.createCustomer();

		Mono<Customer> response = service.createCustomer(request);
		Assert.assertNotNull(response);

		Mockito.verify(repository, Mockito.times(1)).save(Mockito.any(Customer.class));

	}

	@Test
	public void updateCustomerTest() throws Exception {

		Mockito.when(repository.save(Mockito.any(Customer.class))).thenReturn(Mono.empty());

		Customer request = Factory.createCustomer();

		Mono<Customer> response = service.updateCustomer(request);
		Assert.assertNotNull(response);

		Mockito.verify(repository, Mockito.times(1)).save(Mockito.any(Customer.class));

	}

	@Test
	public void deleteCustomerTest() throws Exception {

		Mockito.when(repository.delete(Mockito.any(Customer.class))).thenReturn(Mono.empty());

		Customer request = Factory.createCustomer();

		Mono<Void> response = service.deleteCustomer(request);
		Assert.assertNotNull(response);

		Mockito.verify(repository, Mockito.times(1)).delete(Mockito.any(Customer.class));

	}
}
