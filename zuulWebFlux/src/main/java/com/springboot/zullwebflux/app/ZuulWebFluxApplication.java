package com.springboot.zullwebflux.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@SpringBootApplication
@EnableZuulProxy
public class ZuulWebFluxApplication {

	public static void main(String[] args) {
		SpringApplication.run(ZuulWebFluxApplication.class, args);
	}

}
